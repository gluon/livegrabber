import nl.arttech.osc.Device;
import nl.arttech.osc.DeviceFinder;
import nl.arttech.osc.DeviceFinderListener;

import com.cycling74.max.*;

public class OscBrowse extends MaxObject implements DeviceFinderListener
{
	private static final String[] INLET_ASSIST = new String[]{
		"(bang) output currently known devices again"
	};
	private static final String[] OUTLET_ASSIST = new String[]{
		"(list) attributes of detected devices: name, ip, port, hostname"
	};
	
	DeviceFinder deviceFinder = null;
	
	// -- attribute with custom setter
	private String type = null;
	
	private void setType(String type) {
		if (this.type != null && this.type.equals(type)) return;
		
		if (type.endsWith("._udp") || type.endsWith("._tcp")) {
			this.type = type;
			
			if (deviceFinder != null) {
				deviceFinder.stopDetecting();
			}
			
			int info_idx = getInfoIdx();
			outlet(info_idx, "update");
			outlet(info_idx, "done");
			
			deviceFinder = new DeviceFinder(this, type);
			deviceFinder.startDetecting();
		}
		else {
			doError("The type should end with the protocol after the dot: either ._udp or ._tcp");
		}
	}
	
	// --
	
	public OscBrowse(Atom[] args)
	{
		declareInlets(new int[]{DataTypes.ALL});
		declareOutlets(new int[]{DataTypes.ALL});
		
		declareAttribute("type", null, "setType");
		
		setInletAssist(INLET_ASSIST);
		setOutletAssist(OUTLET_ASSIST);
		
//		future: try to detect existence of osc.jar		
//		try {
//			Class.forName("OscMessage", false, null);
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		setType("_osc._udp");
	}
    
	public void notifyDeleted() {
		if (deviceFinder != null) deviceFinder.stopDetecting();
	}

	public void bang()
	{
		int info_idx = getInfoIdx();
		outlet(info_idx, "update");
		
		if (deviceFinder != null) {
			for(Device device : deviceFinder.getDevices()) {
				if (device.address != null) {
					outlet(0, new Atom[]{Atom.newAtom(device.deviceName), Atom.newAtom(device.address.getHostAddress()), Atom.newAtom(device.port), Atom.newAtom(device.hostName)});
				}
			}
		}
		
		outlet(info_idx, "done");		
	}
    

	public synchronized void devicesUpdated(int resolvesPending) {
//		post("devices updated. resolves pending: " + resolvesPending);
		if (resolvesPending == 0) {
			bang();
		}
	}
	
	
	public void errorFound(String message) {
		doError(message);
	}
	
	private void doError(String message) {
		error("OscBrowser: " + message);
	}
}