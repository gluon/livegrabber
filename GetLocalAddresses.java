import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;
import java.net.*;

import com.cycling74.max.*;


public class GetLocalAddresses extends MaxObject {
	// attribute
	private int reportloopback = 0;
	
	public GetLocalAddresses(Atom[] args) {
		declareAttribute("reportloopback", null, null);

		declareInlets(new int[] {DataTypes.ALL});	
		declareOutlets(new int[] {DataTypes.ALL});	
	}
	
	public void bang() {
		try {
			for (final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces(); interfaces.hasMoreElements();) {
				final NetworkInterface cur = interfaces.nextElement();

				for (Enumeration<InetAddress> e = cur.getInetAddresses(); e.hasMoreElements();) {
					InetAddress address = e.nextElement();

					if (reportloopback == 0 && address.isLoopbackAddress()) {
						break;
					}
					else {
						Vector<Atom> returnAtoms = new Vector<Atom>(0,1);				
						returnAtoms.addAll(Arrays.asList(Atom.parse(cur.getName())));	
						
						if (!(address instanceof Inet4Address)) {
							continue;
						}
						else {
							returnAtoms.addAll(Arrays.asList(Atom.parse(address.getHostAddress())));
						}
						outlet(0, returnAtoms.toArray(new Atom[0]));
					}
				}
			}			
			
//			for (final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces(); interfaces.hasMoreElements();) {
//				final NetworkInterface cur = interfaces.nextElement();
//				
//				for (final InterfaceAddress addr : cur.getInterfaceAddresses()) {
//					
//					if (reportloopback == 0 && cur.isLoopback()) continue;
//					
//					Vector<Atom> returnAtoms = new Vector<Atom>(0,1);				
//					returnAtoms.addAll(Arrays.asList(Atom.parse(cur.getName())));	
//					
//					final InetAddress inet_addr = addr.getAddress();
//					if (!(inet_addr instanceof Inet4Address)) {
//						continue;
//					}
//					else {
//						returnAtoms.addAll(Arrays.asList(Atom.parse(inet_addr.getHostAddress())));
//					}
//					
//					outlet(0, returnAtoms.toArray(new Atom[0]));
//				}
//			}
		}
		catch (SocketException e) {
			System.out.println("Socket Exception");
		}
	}
	
	public void getAddress(String interfaceName) {
		try {
			for (final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces(); interfaces.hasMoreElements();) {
				final NetworkInterface cur = interfaces.nextElement();
				if (cur.isLoopback()) continue;
				
				for (final InterfaceAddress addr : cur.getInterfaceAddresses()) {
					final InetAddress inet_addr = addr.getAddress();
			
					if (!(inet_addr instanceof Inet4Address)) {
						continue;
					}
					else {
						Vector<Atom> returnAtoms = new Vector<Atom>(0,1);				
						returnAtoms.addAll(Arrays.asList(Atom.parse(inet_addr.getHostAddress())));
						outlet(0, returnAtoms.toArray(new Atom[0]));
					}
				}
			}
		}
		catch (SocketException e) {
			System.out.println("Socket Exception");
		}
	}
}















