THIS FORK IS FOR MY OWN USE AND BASED ON MATTIJS KNEPPERS' LIVE GRABBER
I'M NOT SUPPORTING IT HERE, BUT KEEP FREE TO GRAB THINGS, ANYWAY


LiveGrabber
The LiveGrabber plugins are a set of Max For Live plugins that send device parameters, track parameters, clip envelopes and audio analysis information from Ableton Live to any device on the network that supports Open Sound Control (OSC). 

For more info and help, please visit http://showsync.info/livegrabber
Made by Mattijs Kneppers (www.arttech.nl)

