import java.util.Vector;

import com.cycling74.max.*;

import nl.arttech.osc.EventScheduler;
import nl.arttech.osc.OscPacket;
import nl.arttech.osc.OscMessage;
import nl.arttech.osc.OscServer;
import nl.arttech.osc.OscServerListener;
import nl.arttech.osc.PacketOutputter;
import nl.arttech.osc.Utils;

public class OscReceive extends MaxObject implements OscServerListener, PacketOutputter
{
	private static final String[] INLET_ASSIST = new String[]{
		"(port int) set port number to listen to"
	};
	private static final String[] OUTLET_ASSIST = new String[]{
		"(symbol list) /address values"
	};
	
	OscServer inputServer = null;
	private MaxQelem outputPortLow;
	private MaxQelem checkAttributesLow;

	// -- attribute with custom setter
	private int port = -1;
	private String device = null;
	private int scheduling = 1;
	
	@SuppressWarnings("unused")
	private void setPort(int port) {
		if (port > 1024 || port == 0) {
			if (inputServer == null || (inputServer != null && inputServer.receivePort != port)) {

				String device = null;
				if (inputServer != null) {
					OscServer.unregisterListener(this);
					device = inputServer.device;
				}

				inputServer = OscServer.registerListener(port, device, this);
				this.port = inputServer.receivePort;
				outputPortLow.set();
			}
		}
		else {
			doError("The port number should be larger than 1024");
		}
	}	
	
	@SuppressWarnings("unused")	
	private int getPort() {
		if (inputServer != null) return inputServer.receivePort;
		else return -1;
	}
	
	@SuppressWarnings("unused")
	private void setDevice(String device) {
		if (!device.equals("")) {
			if (Utils.getAddress(device) != null) {
				if (inputServer == null || inputServer.device == null || (inputServer != null && !inputServer.device.equals(device))) {
					
					int port = 0;
					if (inputServer != null) {
						port = inputServer.receivePort;
						OscServer.unregisterListener(this);
					}
					
					inputServer = OscServer.registerListener(port, device, this);
					this.device = inputServer.device;
				}
			}
			else {
				doError("" + device + " is not an existing network device");
			}
		}
		else {
			if (inputServer != null && inputServer.device != null) {
				int port = inputServer.receivePort;
				
				OscServer.unregisterListener(this);
				inputServer = OscServer.registerListener(port, null, this);
				this.device = null;
				//doError("The device name should be a valid string");
			}
		}
	}
	
	@SuppressWarnings("unused")		
	private String getDevice() {
		if (inputServer != null && inputServer.device != null) return inputServer.device;
		else return "all";
	}
	
	@SuppressWarnings("unused")
	private void outputPort() {
		int info_idx = getInfoIdx();
		outlet(info_idx, "port", inputServer.receivePort);
	}
	
	// --
	
	public OscReceive(Atom[] args) {
		outputPortLow = new MaxQelem(new Callback(this,"outputPort"));
		checkAttributesLow = new MaxQelem(new Callback(this,"checkAttributes"));
		
		declareInlets(new int[]{DataTypes.ALL});
		declareOutlets(new int[]{DataTypes.ALL});
		
		declareAttribute("port", "getPort", "setPort");
		declareAttribute("device", "getDevice", "setDevice");
		declareAttribute("scheduling", null, null);
		
		setInletAssist(INLET_ASSIST);
		setOutletAssist(OUTLET_ASSIST);
		
		checkAttributesLow.set();
	}
	
	@SuppressWarnings("unused")
	private void checkAttributes() {
		if (port == -1) {
			post("OscReceive: No port attribute specified, going for any free port. Info outlet will report which port was chosen.");
			inputServer = OscServer.registerListener(0, device, this);
			outputPortLow.set();
		}
	}
    
	public void notifyDeleted() {
		if (inputServer != null) OscServer.unregisterListener(this);
		outputPortLow.release();
		checkAttributesLow.release();
	}

	public void bang() {
		//yo
	}
	
	
	public void errorFound(String message) {
		doError(message);
	}
	

	public void incomingOscPacket(OscPacket packet) {
//		post("incoming: " + packet);
		long receiveTime = System.currentTimeMillis();
		long packetTime = OscPacket.timestampToMs(packet.ntpTimeTag);
		if (packetTime <= receiveTime || scheduling == 0) {
			outputPacket(packet, receiveTime);
//			MaxSystem.scheduleDelay(new EventScheduler(packet, this, now), 0);
		}
		else {
			MaxSystem.scheduleDelay(new EventScheduler(packet, this, receiveTime), packetTime - System.currentTimeMillis());
		}
	}
	
	public void outputPacket(OscPacket packet, long receiveTime) {
		int info_idx = getInfoIdx();
	
		for (OscMessage message : packet.messages) {
			Vector<Atom> values = message.argsToAtoms();
			Atom[] output = new Atom[values.size() + 1];
			output[0] = Atom.newAtom(message.address); 
			int i = 1;
			for (Atom value: values) {
				output[i++] = value;
			}
			
			if (scheduling == 1) {
				outletHigh(info_idx, new Atom[] {Atom.newAtom("source"), Atom.newAtom(packet.address.getHostAddress())});
				outletHigh(info_idx, new Atom[] {Atom.newAtom("timeDiff"), (packet.ntpTimeTag != OscPacket.timeZeroBytes) ? Atom.newAtom(receiveTime - OscPacket.timestampToMs(packet.ntpTimeTag)) : Atom.newAtom("immediate")});
				outletHigh(0, output);
			}
			else {
				outlet(info_idx, new Atom[] {Atom.newAtom("source"), Atom.newAtom(packet.address.getHostAddress())});
				outlet(info_idx, new Atom[] {Atom.newAtom("timeDiff"), (packet.ntpTimeTag != OscPacket.timeZeroBytes) ? Atom.newAtom(receiveTime - OscPacket.timestampToMs(packet.ntpTimeTag)) : Atom.newAtom("immediate")});
				outlet(0, output);
			}
		}
	}

	private void doError(String message) {
		error("OscReceive: " + message);
	}
}