import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.PortUnreachableException;
import java.net.SocketException;
import java.net.UnknownHostException;

import nl.arttech.osc.OscMessage;
import nl.arttech.osc.OscPacket;

import com.cycling74.max.*;

public class OscSend extends MaxObject
{
	private static final String[] INLET_ASSIST = new String[]{
		"(list) /address values"
	};
//	private static final String[] OUTLET_ASSIST = new String[]{
//		"Nothing yet"
//	};
	
    DatagramSocket oscSendSocket;
	OscPacket packet = new OscPacket();
	InetAddress localAddress = null;
	
	// -- attribute with custom setter
	private String host = "127.0.0.1";
	private int port = -1;
	private int timeoffset = 0;
	boolean usetimetag = true;
	
	
	
	// --
	@SuppressWarnings("unused")
	private void setPort(int port) {
		if (port > 1024) {
			this.port = port;
		}
		else {
			doError("The port number should be larger than 1024");
		}
	}
	
 	public OscSend(Atom[] args) {
		declareInlets(new int[]{DataTypes.ALL});
//		declareOutlets(new int[]{DataTypes.ALL});
		
		declareAttribute("host", null, null);
		declareAttribute("port", null, "setPort");
		declareAttribute("timeoffset", null, null);
		declareAttribute("usetimetag", null, null);
			
		
		setInletAssist(INLET_ASSIST);
		
		try {
			oscSendSocket = new DatagramSocket();
		} catch (SocketException e) {
			doError("Problem opening socket");
		}
//		setOutletAssist(OUTLET_ASSIST);
	}
    
	public void notifyDeleted() {
		oscSendSocket.close();
	}

	public void bang() {
		if (port > 1024) {
			sendPacket(packet);
		}
		packet = new OscPacket();
    }		

	
	private void sendPacket(OscPacket oscPacket) {
		try {
			oscPacket.address = InetAddress.getByName(host);
		} catch (UnknownHostException e1) {
			doError("Problem preparing address for sending");
			return;
		}
		oscPacket.port = port;
		if (usetimetag) {
			oscPacket.ntpTimeTag = OscPacket.msToTimestamp(System.currentTimeMillis() + timeoffset);
		}
		else {
			oscPacket.ntpTimeTag = OscPacket.timeZeroBytes;
		}
		
		byte[] packetBytes = {0};
		try {
			//post("" + oscPacket);
			packetBytes = oscPacket.getByteArray();
		} catch (IOException e) {
			doError("getByteArray failed");
			e.printStackTrace();
		}
		
		DatagramPacket packet = new DatagramPacket(packetBytes, packetBytes.length, oscPacket.address, oscPacket.port);
//		post("___Sending UDP packet___");
//		post(oscPacket.toString());
		
		try {
			oscSendSocket.send(packet);
		} catch (PortUnreachableException e) {
			int info_idx = getInfoIdx();
			outlet(info_idx, "failed", packet.getAddress().toString());
		} catch (SecurityException e) {
			doError("Sending packet to " + packet.getAddress() + " failed: " + e.getMessage() + " due to a Security Exception.");
		}
		catch (IOException e) {
			doError("Sending packet to " + packet.getAddress() + " failed: " + e.getMessage() + " due to an IO Error.");
		}
	}
	
	public void anything(String msg, Atom[] args) {
		if (msg.startsWith("/")) {
			if (!msg.endsWith("/")) {
				OscMessage message = new OscMessage(msg);
				message.atomsToArgs(args);
				packet.addMessage(message);
			}
			else {
				doError("Address cannot end with a slash (/)");
			}
		}
		else {
			doError("Address should start with a slash (/)");
		}
	}

	private void doError(String message) {
		error("OscSend: " + message);
	}	
}