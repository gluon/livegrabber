import java.net.InetAddress;

import nl.arttech.osc.ServicePublisher;
import nl.arttech.osc.ServicePublisherListener;
import nl.arttech.osc.Utils;

import com.cycling74.max.Atom;
import com.cycling74.max.DataTypes;
import com.cycling74.max.MaxObject;

public class OscService extends MaxObject implements ServicePublisherListener {

	private static final String[] INLET_ASSIST = new String[]{
		"set attributes"
	};
	private static final String[] OUTLET_ASSIST = new String[]{
		"(symbol) actual name"
	};
	
	private boolean active = false;
	
	// -- attribute with custom setter
	String device = null;
	
	private String type = "_osc._udp";
	@SuppressWarnings("unused")
	private void setType(String type) {		
		if (validType(type)) {
			this.type = type;
			
			if (publisher != null) {
				publisher.stop();
			}
			publisher = new ServicePublisher(type, this);
			
			if (active) doPublish();
		}
		else {
			doError("Invalid type. The type should end with the protocol after the dot: either ._udp or ._tcp");
		}
	}
	
	@SuppressWarnings("unused")	
	private void setDevice(String device) {
		if (!device.equals("")) {
			InetAddress localDeviceAddress = Utils.getAddress(device);
			if (localDeviceAddress != null) {
				if (this.device == null || (this.device != null && !this.device.equals(device))) {
					this.device = device;
					if (active) doPublish();
				}
			}
			else {
				doError("" + device + " is not an existing network device");
			}
		}
		else {
			if (this.device != null) {
				this.device = null;
				if (active) doPublish();
			}
		}
	}
	
	@SuppressWarnings("unused")		
	private String getDevice() {
		return device;
	}	
	
	// --
	
	private String name = null;
	private int port = -1;
	
	// --
	
	ServicePublisher publisher = null;
	
	public OscService(Atom[] args)
	{	
		declareInlets(new int[]{DataTypes.ALL});
		declareOutlets(new int[]{DataTypes.ALL});
		
		setInletAssist(INLET_ASSIST);
		setOutletAssist(OUTLET_ASSIST);
		
		declareAttribute("type", null, "setType");
		declareAttribute("device", "getDevice", "setDevice");
		
		if (args.length > 0) updateArgs(args);
		
		if (validType(type)) {
			publisher = new ServicePublisher(type, this);
			
			doPublish();
		}
		else {
			doError("Invalid type");
		}
	}
	
	public void notifyDeleted() {
		if (publisher != null) publisher.stop();
	}

	boolean validType(String type) {
		if (type != null && (type.endsWith("._udp") || type.endsWith("._tcp"))) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean updateArgs(Atom[] args) {
		if (args.length != 2) {
			doError("Service requires two arguments: host (symbol) and port (int)");
			return false;
		}
		else if (!(args[0].isString() && args[1].isInt())) {
			doError("Service requires two arguments: host (symbol) and port (int)");
			return false;
		}
		else {
			if (args[1].getInt() > 1024) {
				this.name = args[0].getString();
				this.port = args[1].getInt();
				if (this.name != null && this.port != 0) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				doError("The port number has to be larger than 1024");
				return false;
			}
		}		
	}
	
	public void service(Atom[] args) {
		if (updateArgs(args)) {
			doPublish();
		}
	}
	
	public void doPublish() {
		if (this.name != null && this.port != -1) {		
			if (validType(type)) {
				if (publisher != null) {
					String deviceName = null;
					if (device == null) {
						String[] interfaceNames = Utils.getInterfaceNames();
						if (interfaceNames.length > 1) {
//							post("OscService: Multiple active network devices found and no device attribute set. Binding to a random one: " + interfaceNames[0]);
							deviceName = interfaceNames[0];
						}
						else if (interfaceNames.length == 1) {
							deviceName = interfaceNames[0];
						}
						else {
							post("OscService: No external network devices found. Binding to anything that is available.");
						}
					}
					else {
						deviceName = device;
					}
					active = true;
					publisher.doRegistration(name, deviceName, port);
				}
				else {
					doError("Publisher not valid");
				}
			}
			else {
				doError("No valid type set");
			}	
		}
		else {
			doError("Port or name not valid");
		}
	}
	

	public void registerDone(String serviceName, int port) {
		outlet(0, serviceName);
	}

	public void stop() {
		if (publisher != null) { 
			publisher.stop();
			active = false;
		}
	}
	

	public void errorFound(String message) {
		doError(message);
	}

	private void doError(String message) {
		error("OscService: " + message);
	}
}
